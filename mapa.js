function cargarMapa() {
    let mapa = L.map('divMapa', { center: [4.625770, -74.172777], zoom: 16 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    let unMarcador = L.marker([4.625770, -74.172777]);
    unMarcador.addTo(mapa);

    let unCirculo = L.circle([4.625770, -74.172777], {
        color: 'blue',
        fillColor: 'black',
        fillOpacity: 0.8,
        radius: 500
    });


    unCirculo.addTo(mapa);

    var polygon = L.polygon([
        [4.62, -74.178],
        [4.623, -74.173],
        [4.625, -74.174],
        [4.624, -74.176]
    ]).addTo(mapa);



}